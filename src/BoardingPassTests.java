import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BoardingPassTests {
    @Test
    public void has_a_row_and_seat() {
        BoardingPass bPass = new BoardingPass(1, 2);
        assertEquals(bPass.row, 1);
        assertEquals(bPass.seat, 2);
    }
}
