import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class AirportTests {
    @Test
    public void a_airport_has_planes() {
        Airport LHR = new Airport("LHR");
        Plane plane = new Plane("LA992");
        LHR.land(plane);
        assertEquals(LHR.getFlight("LA992"), plane);
    }

    @Test
    public void after_departures_passenger_has_boarding_pass() {
        Airport LHR = new Airport("LHR");
        Passenger passenger = new Passenger(new Flight("LHR", "LAX", "LA992"));
        Plane plane = new Plane("LA992");
        LHR.land(plane);
        LHR.departures(passenger);
        assertEquals(passenger.getRow(), 0);
    }

    @Test
    public void a_airport_has_departures() {
        Airport LHR = new Airport("LHR");
        Passenger passenger = new Passenger(new Flight("LHR", "LAX", "LA992"));
        Plane plane = new Plane("LA992");
        LHR.land(plane);
        LHR.departures(passenger);
        Plane _plane = LHR.getFlight("LA992");
        assertTrue(_plane.hasPassenger(passenger));
    }

    @Test
    public void can_access_all_airports() {
        new Airport("LHR");
        Airport LAX = new Airport("LAX");
        assertEquals(Airport.all.get("LAX"), LAX);
        assertEquals(Airport.all.size(), 2);
    }

    @Test
    public void you_can_take_off_and_fly_to_another_airport() {
        Airport LHR = new Airport("LHR");
        Airport LAX = new Airport("LAX");
        Passenger passenger = new Passenger(new Flight("LHR", "LAX", "LA992"));
        Plane plane = new Plane("LA992");
        LHR.land(plane);
        LHR.departures(passenger);
        LHR.takeOff("LA992");
        assertNull(LHR.getFlight("LA992"));
        assertNotNull(LAX.getFlight("LA992"));
        assertEquals(LAX.getFlight("LA992"), plane);    
    }
}
