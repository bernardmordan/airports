public class BoardingPass {
    public int row;
    public int seat;

    public BoardingPass(int row, int seat) {
        this.row = row;
        this.seat = seat;
    }
}
