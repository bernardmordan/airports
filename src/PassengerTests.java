import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

public class PassengerTests {
    @Test
    public void a_person_has_a_flight () {
        Passenger passenger = new Passenger(new Flight("LHR", "LAX", "LA992"));
        assertEquals(passenger.getInbound(), "LAX");
    }

    @Test
    public void a_person_has_a_flightNo () {
        Passenger passenger = new Passenger(new Flight("LHR", "LAX", "LA992"));
        assertEquals(passenger.getFlightNo(), "LA992");
    }

    @Test
    public void has_a_boarding_pass() {
        Passenger passenger = new Passenger(new Flight("LHR", "LAX", "LA992"));
        BoardingPass bPass = new BoardingPass(2, 3);
        passenger.setBoardingPass(bPass);
        assertEquals(passenger.getRow(), 2);
        assertEquals(passenger.getSeat(), 3);
    }
}
