public class Flight {
    public String outBound;
    public String inBound;
    public String flightNo;

    public Flight(String outBound, String inBound, String flightNo) {
        this.outBound = outBound;
        this.inBound = inBound;
        this.flightNo = flightNo;
    }
}
