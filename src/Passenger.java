public class Passenger {
    private Flight flight;
    private BoardingPass boardingPass;

    public Passenger(Flight flight) {
        this.flight = flight;
        this.boardingPass = null;
    }

    public String getInbound() {
        return this.flight.inBound;
    }

    public String getFlightNo() {
        return this.flight.flightNo;
    }

    public void setBoardingPass(BoardingPass boardingPass) {
        this.boardingPass = boardingPass;
    }

    public int getRow() {
        return this.boardingPass == null ? null : this.boardingPass.row;
    }

    public int getSeat() {
        return this.boardingPass == null ? null : this.boardingPass.seat;
    }
}
