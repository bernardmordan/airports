import java.util.HashMap;

public class Airport {
    public static HashMap<String, Airport> all = new HashMap<>();

    private String code;
    private HashMap<String, Plane> planes;

    public Airport(String code) {
        this.code = code;
        this.planes = new HashMap<>();
        Airport.all.put(code, this);
    }

    public Plane getFlight(String flightNo) {
        return this.planes.get(flightNo);
    }

    public void departures(Passenger passenger) {
        Plane plane = this.planes.get(passenger.getFlightNo());
        int[] rowAndSeat = plane.getNextFreeSeat();
        BoardingPass bPass = new BoardingPass(rowAndSeat[0], rowAndSeat[1]);
        passenger.setBoardingPass(bPass);
        plane.board(passenger);
    }

    public void land(Plane plane) {
        this.planes.put(plane.getFlightNo(), plane);
    }

    public void takeOff(String flightNo) {
        // 1. get the plane from this airport
        Plane plane = this.planes.get(flightNo);
        // 2. figure out where the plane is going
        String inBound = plane.getInBound();
        // 3. Remove plane from this.planes
        this.planes.remove(flightNo);
        // 4. land the plane at destination airport
        Airport destination = Airport.all.get(inBound);
        destination.land(plane);
    }
}
