import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PlaneTests {
    @Test
    public void plane_has_free_seats() {
        Plane plane = new Plane("LA992");
        int[] rowAndSeat = plane.getNextFreeSeat();
        assertEquals(rowAndSeat[0], 0);
        assertEquals(rowAndSeat[1], 0);
    }
    @Test
    public void plane_has_free_seats_after_adding_a_passenger() {
        Plane plane = new Plane("LA992");
        Passenger passenger1 = new Passenger(new Flight("LHR", "LAX", "LA992"));
        BoardingPass bPass1 = new BoardingPass(0, 0);
        passenger1.setBoardingPass((bPass1));
        plane.board(passenger1);
        int[] rowAndSeat = plane.getNextFreeSeat();
        assertEquals(rowAndSeat[0], 0);
        assertEquals(rowAndSeat[1], 1);
    }
    @Test
    public void has_an_inBound_airport() {
        Plane plane = new Plane("LA992");
        Passenger passenger = new Passenger(new Flight("LHR", "LAX", "LA992"));
        BoardingPass bp = new BoardingPass(0, 0);
        passenger.setBoardingPass(bp);
        plane.board(passenger);
        assertEquals(plane.getInBound(), "LAX");
    }
}
